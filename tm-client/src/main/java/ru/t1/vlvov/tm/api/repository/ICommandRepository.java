package ru.t1.vlvov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String argument);

    @Nullable
    Collection<AbstractCommand> getTerminalCommands();

    @Nullable
    Collection<AbstractCommand> getCommandsWithArgument();

}
