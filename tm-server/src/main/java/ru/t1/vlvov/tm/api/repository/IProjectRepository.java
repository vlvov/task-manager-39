package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO PROJECT (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})")
    void addProject(@NotNull Project project);

    @Delete("DELETE FROM PROJECT WHERE USER_ID = #{userId}")
    void clearProjectUserOwned(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE PROJECT")
    void clearProject();

    @Select("SELECT * FROM PROJECT")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<Project> findAllProject();

    @Select("SELECT * FROM PROJECT WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    Project findProjectById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM PROJECT WHERE ID = #{id}")
    void removeProject(@NotNull Project project);

    @Delete("DELETE FROM PROJECT WHERE ID = #{id}")
    void removeProjectById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM PROJECT ORDER BY NAME")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Project> findAllProjectSortByName(@NotNull Comparator comparator);

    @Select("SELECT * FROM PROJECT WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Project> findAllProjectUserOwned(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM PROJECT WHERE USER_ID = #{userId} AND ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    Project findProjectByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM PROJECT WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeProjectByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM PROJECT WHERE USER_ID = #{userId} ORDER BY CREATED")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Project> findAllProjectSortByCreated(@Param("userId") @NotNull String userId, @NotNull Comparator comparator);

    @Update("UPDATE PROJECT SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status} WHERE ID = #{id}")
    void updateProject(@NotNull Project project);

    @Update("UPDATE PROJECT SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status} WHERE ID = #{id} AND USER_ID = #{userId}")
    void updateProjectUserOwned(@NotNull Project project);

}
