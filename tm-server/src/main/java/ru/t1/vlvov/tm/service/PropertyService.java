package ru.t1.vlvov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    final String FILE_NAME = "application.properties";

    @NotNull
    final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    final String DB_USER = "database.user";

    @NotNull
    final String DB_USER_DEFAULT = "postgres";

    @NotNull
    final String DB_PASSWORD = "database.password";

    @NotNull
    final String DB_PASSWORD_DEFAULT = "admin";

    @NotNull
    final String DB_URL = "database.url";

    @NotNull
    final String DB_DRIVER = "database.driver";

    @NotNull
    final String SERVER_PORT_KEY = "server.port";

    @NotNull
    final String SERVER_HOST_KEY = "server.host";

    @NotNull
    final String SESSION_KEY = "session.key";

    @NotNull
    final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    final String PASSWORD_SECRET_DEFAULT = "5926";

    @NotNull
    final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    final String PASSWORD_ITERATION_DEFAULT = "3156";

    @NotNull
    final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    final String EMPTY_VALUE = "---";

    @NotNull
    final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return Integer.parseInt(getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getDatabaseUser() {
        return getStringValue(DB_USER, DB_USER_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabasePassword() {
        return getStringValue(DB_PASSWORD, DB_PASSWORD_DEFAULT);
    }

    @Override
    @NotNull
    public String getDatabaseUrl() {
        return getStringValue(DB_URL, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, EMPTY_VALUE);
    }

    @Override
    public @NotNull String getSessionKey() {
        return getStringValue(SESSION_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public Integer getSessionTimeout() {
        return Integer.parseInt(getStringValue(SESSION_TIMEOUT_KEY, EMPTY_VALUE));
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER, EMPTY_VALUE);
    }

}
