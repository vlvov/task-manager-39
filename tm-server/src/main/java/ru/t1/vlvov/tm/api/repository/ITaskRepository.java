package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO TASK (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID, PROJECT_ID)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void addTask(@NotNull Task task);

    @Delete("DELETE FROM TASK WHERE USER_ID = #{userId}")
    void clearTaskUserOwned(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE TASK")
    void clearTask();

    @Select("SELECT * FROM TASK")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<Task> findAllTask();

    @Select("SELECT * FROM TASK WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    Task findTaskById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM TASK WHERE ID = #{id}")
    void removeTask(@NotNull Task task);

    @Delete("DELETE FROM TASK WHERE ID = #{id}")
    void removeTaskById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM TASK ORDER BY NAME")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Task> findAllTaskSortByName(@NotNull Comparator comparator);

    @Select("SELECT * FROM TASK WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Task> findAllTaskUserOwned(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM TASK WHERE USER_ID = #{userId} AND ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    Task findTaskByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TASK WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeTaskByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM TASK WHERE USER_ID = #{userId} ORDER BY CREATED")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Task> findAllTaskSortByCreated(@Param("userId") @NotNull String userId, @NotNull Comparator comparator);

    @Update("UPDATE TASK SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status}, PROJECT_ID = #{projectId} WHERE ID = #{id}")
    void updateTask(@NotNull Task task);

    @Update("UPDATE TASK SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status}, PROJECT_ID = #{projectId} WHERE ID = #{id} AND USER_ID = #{userId}")
    void updateTaskUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM TASK WHERE PROJECT_ID = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<Task> findAllByProjectId(@Param("projectId") @NotNull String projectId);

    @Select("SELECT * FROM TASK WHERE USER_ID = #{userId} AND PROJECT_ID = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<Task> findAllByProjectIdUserOwned(@Param("userId") @NotNull String userId, @Param("projectId") String projectId);

}
