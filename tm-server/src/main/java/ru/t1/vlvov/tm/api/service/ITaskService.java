package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @Nullable
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @NotNull
    @SneakyThrows
    Task add(@Nullable Task model);

    @NotNull
    @SneakyThrows
    Collection<Task> add(@Nullable Collection<Task> tasks);

    @NotNull
    @SneakyThrows
    Collection<Task> set(@Nullable Collection<Task> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<Task> findAll();

    @NotNull
    @SneakyThrows
    Task remove(@Nullable Task task);

    @Nullable
    @SneakyThrows
    Task removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    Task findOneById(@Nullable String id);

    @Nullable
    @SneakyThrows
    List<Task> findAll(@Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    Task add(@Nullable String userId, @Nullable Task task);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<Task> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Task remove(@Nullable String userId, @Nullable Task task);

    @Nullable
    @SneakyThrows
    Task removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    List<Task> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @NotNull String description);

    @Nullable
    @SneakyThrows
    List<Task> findAllByProjectId(@Nullable String projectId);
}
