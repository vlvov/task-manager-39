package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import static ru.t1.vlvov.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vlvov.tm.constant.TaskTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clearTaskUserOwned(USER1.getId());
            taskRepository.clearTaskUserOwned(USER2.getId());
            taskRepository.clearTaskUserOwned(ADMIN1.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            Assert.assertNull(taskRepository.findTaskById(USER1_TASK1.getId()));
            taskRepository.addTask(USER1_TASK1);
            sqlSession.commit();
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findTaskById(USER1_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.addTask(USER1_TASK1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), taskRepository.findAllTask().get(0).getUserId());
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findAllTask().get(0).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.addTask(USER1_TASK1);
            taskRepository.addTask(USER2_TASK1);
            sqlSession.commit();
            taskRepository.clearTaskUserOwned(USER1.getId());
            sqlSession.commit();
            Assert.assertNull(taskRepository.findTaskById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findTaskById(USER2_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            for (final Task task : TASK_LIST) {
                taskRepository.addTask(task);
            }
            sqlSession.commit();
            Assert.assertFalse(taskRepository.findAllTaskUserOwned(USER1.getId()).isEmpty());
            taskRepository.clearTaskUserOwned(USER1.getId());
            sqlSession.commit();
            Assert.assertTrue(taskRepository.findAllTaskUserOwned(USER1.getId()).isEmpty());
            Assert.assertFalse(taskRepository.findAllTask().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.addTask(USER1_TASK1);
            taskRepository.addTask(USER2_TASK1);
            sqlSession.commit();
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findTaskByIdUserOwned(USER1.getId(), USER1_TASK1.getId()).getId());
            Assert.assertNull(taskRepository.findTaskByIdUserOwned(USER1.getId(), USER2_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.addTask(USER1_TASK1);
            taskRepository.addTask(USER2_TASK1);
            sqlSession.commit();
            taskRepository.removeTaskByIdUserOwned(USER1.getId(), USER1_TASK1.getId());
            sqlSession.commit();
            Assert.assertNull(taskRepository.findTaskById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findTaskById(USER2_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.addTask(USER1_TASK1);
            taskRepository.addTask(USER2_TASK1);
            sqlSession.commit();
            taskRepository.removeTaskByIdUserOwned(USER1.getId(), USER1_TASK1.getId());
            sqlSession.commit();
            Assert.assertNull(taskRepository.findTaskById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findTaskById(USER2_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.addTask(USER1_TASK1);
            sqlSession.commit();
            Assert.assertNotNull(taskRepository.findTaskById(USER1_TASK1.getId()));
            Assert.assertNull(taskRepository.findTaskById(USER2_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByProjectIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            for (final Task task : USER1_TASK_LIST) {
                taskRepository.addTask(task);
            }
            for (final Task task : USER2_TASK_LIST) {
                taskRepository.addTask(task);
            }
            sqlSession.commit();
            Assert.assertFalse(taskRepository.findAllByProjectIdUserOwned(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
